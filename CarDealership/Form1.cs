﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarDealership
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            cmbYear.Items.AddRange(GetYears());
            cmbYear.SelectedIndex = 0;
        }

        private string[] GetYears()
        {
            return new string[]
            {
                "Please Select Year",
                "2018",
                "2019"
            };
        }

        private string[] GetMakes(string year)
        {
            switch(year)
            {
                default:
                    return new string[] { "Please Select Year" };
                case "2018":
                    return new string[]
                    {
                        "Please Select Make",
                        "Buick",
                        "Chevrolet",
                        "Ford"
                    };
                case "2019":
                    return new string[]
                   {
                        "Please Select Make",
                        "Buick",
                        "Chevrolet",
                        "Ford",
                        "Tesla"
                   };

            }
        }

        private Car[] GetModels(string year,string make)
        {
            switch($"{year} {make}")
            {
                default:
                    return new Car[0];
                        
                case "2018 Buick":
                    return new Car[]
                    {
                        new Car{Year="2018", Make="Buick", Model="Encore",
                            Image =CarDealership.Properties.Resources.BuickEncore2018},
                        new Car{Year="2018", Make="Buick", Model="LaCrosse",
                        Image=CarDealership.Properties.Resources._2018LaCrosse}
                    };
                case "2019 Buick":
                    return new Car[]
                    {
                        new Car{Year="2019", Make="Buick", Model="Encore",
                        Image=CarDealership.Properties.Resources._2019Encore}
                    };
            }
        }

        private void cmbYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            string year = (string)cmbYear.SelectedItem;
            cmbMake.Items.Clear();
            
            cmbMake.Items.AddRange(GetMakes(year));
            cmbMake.SelectedIndex = 0;
        }

        private void cmbMake_SelectedIndexChanged(object sender, EventArgs e)
        {
            string year = (string)cmbYear.SelectedItem;
            string make = (string)cmbMake.SelectedItem;

            cmbModel.Items.Clear();
            cmbModel.Items.AddRange(GetModels(year, make));
            
            //lblCarName.Text = "";
            //lblCarDescription.Text = "";
            //imgCarImage.Image = null;
        }

        private void cmbModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            Car c = (Car)cmbModel.SelectedItem;
            lblCarName.Text = c.Name;
            lblCarDescription.Text = c.Description;
            imgCarImage.Image = c.Image;
        }
    }
}
