﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGDemo
{
   public class Dog
    {
        //Instance Fields
        private string name;
        private int age;

        //Creating my own constructor
        public Dog(string name)
        {
            this.name = name;
        }

        public Dog(string name, int age)
        {
            this.name = name;
            this.age = age;
        }

        public Dog()
        {

        }


        //Properties
        public String Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public int Age
        {
            get
            {
                return age;
            }
            set
            {
                if(value < 0 || value > 100)
                {
                    //bad data
                }
                else
                {
                    //Good data
                    this.age = value;
                }
            }
        }

        //Auto-Implemented Property
        public string Breed { get; set; }

        //Bark is an instance method
        //aka it's just non-static
        //Inside the parens we have parameters
        public string Bark(int numSeconds)
        {
            //When a variable is declared in 
            //a method it's called a local variable
            return "This dog is barking for " + numSeconds + " seconds.";

        }
    }
}
