﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EGDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //Declaring a variable 
            //who's data type is a class
            //of dog
            Dog myDog;

            //Instantiating the variable
            //by calling a method
            //Dog();
            //aka the constructor
            myDog = new Dog();

            //Cat aCat = new Cat();

            myDog.Name = "Cali";
            myDog.Breed = "Shih Tzu";
            Console.WriteLine("My dog's name is {0}", myDog.Name);
            Console.WriteLine(myDog.Bark(10));
            Console.WriteLine(myDog.Breed);

            Dog yourDog = new Dog();
            yourDog.Name = "Rocky";
            yourDog.Age = 200;
            yourDog.Breed = "Pitbull";

            Console.WriteLine("Your dog's name is {0}", yourDog.Name);
            Console.WriteLine(yourDog.Bark(100));
            Console.WriteLine("Your dogs age is " + yourDog.Age);


            Dog strayDog = new Dog("HungryBoi");

            Console.WriteLine(strayDog.Name);

            strayDog.Name = "GoodBoi";

            Console.WriteLine(strayDog.Name);

            Dog snoopDog = new Dog("Snoop", 47);

            Console.WriteLine("{0} dog is {1} years old", snoopDog.Name, snoopDog.Age);

        }
    }
}
