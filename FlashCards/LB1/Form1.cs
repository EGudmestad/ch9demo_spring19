﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB1
{
    public partial class Form1 : Form
    {

        FlashCard[] myCards;
        int count = 5;
        public Form1()
        {
            InitializeComponent();
            myCards = new FlashCard[20];
            myCards[0] = new FlashCard("HTML", "Hypertext Markup Language");
            myCards[1] = new FlashCard("CSS", "Cascading Style Sheet");
            myCards[2] = new FlashCard("JS", "JavaScript");
            myCards[3] = new FlashCard("C#", "C Sharp Programming");
            myCards[4] = new FlashCard("VB", "Visual Basic");
        }

        private void btnDefine_Click(object sender, EventArgs e)
        {
            lblSuccess.Text = "";
            string input = txtTerm.Text;
            bool found = false;
            for (int i = 0; i < count ; i++)
            {
                if(input.ToUpper().Equals(myCards[i].GetTerm()))
                {
                    lblDefinition.Text = myCards[i].GetDefinition();
                    found = true;
                }
            }

            if(found == false)
            {
                lblUnknown.Text = "I don't know that term yet, what does it mean?";
                txtDefinition.Visible = true;
                btnAdd.Visible = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            myCards[count] = new FlashCard(txtTerm.Text, txtDefinition.Text);
            count++;
            lblSuccess.Text = "Flash Card Added.";
            lblUnknown.Text = "";
            txtDefinition.Visible = false;
            btnAdd.Visible = false;
        }


       



    }
}
