﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo
{
    class Dog : Animal
    {
        public string CoatColor { get; set; }

        public Dog(string name, int age,string cc) : base(name,age)
        {
            CoatColor = cc;
        }

        public override void Move()
        {
            base.Move();
            Console.WriteLine("A dog gallops");
        }

        public override string ToString()
        {
            return "My age is " + Age + " my name is " +
                Name + " my fur's color is " + CoatColor;
        }
    }
}
