﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo
{
    class Snake : Animal
    {
        public override void Move()
        {
            Console.WriteLine("A snake slithers...");
        }
    }
}
