﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB11
{
    public partial class Form1 : Form
    {
        double orderTotal = 0;
        public Form1()
        {
            InitializeComponent();
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lblSundaeError.Text = "";
            lblSodaError.Text = "";
            lblNameError.Text = "";

            string orderName = txtName.Text;

            bool hasSundae = chkSundae.Checked;
            bool hasSoda = chkSoda.Checked;

           

 
            Order anOrder = new Order(orderName, hasSundae, hasSoda);
           // int sundaeToppingCount=0;

            //INPUT
            if(hasSundae)
            { 
                if (chkSprinkles.Checked)
                {
                    anOrder.Sundae.AddTopping(SundaeTopping.SPRINKLES);
                   // sundaeToppingCount++;
                }

                if (chkNuts.Checked)
                {
                    anOrder.Sundae.AddTopping(SundaeTopping.NUTS);
                   // sundaeToppingCount++;
                }

                if (chkChocolateSyrup.Checked)
                {
                    anOrder.Sundae.AddTopping(SundaeTopping.CHOCOLATE_SYRUP);
                   // sundaeToppingCount++;
                }

                //if(anOrder.Sundae.ToppingCount == 0)
                //{
                //    anOrder.Sundae.AddTopping(SundaeTopping.NONE);
                //}
            }

            if (hasSoda)
            {
                int numFlavors = 0;

                if (chkLime.Checked)
                {
                    numFlavors++;
                }
                if(chkPeach.Checked)
                {
                    numFlavors++;
                }
                if(chkMango.Checked)
                {
                    numFlavors++;
                }

                if (numFlavors > 1)
                {
                    lblSodaError.Text = "Only 1 mixin allowed";
                    return;
                }

                if (chkLime.Checked)
                {
                    anOrder.Soda.AddFlavor(SodaFlavor.LIME);
                }
                if (chkPeach.Checked)
                {
                    anOrder.Soda.AddFlavor(SodaFlavor.PEACH);
                }
                if (chkMango.Checked)
                {
                    anOrder.Soda.AddFlavor(SodaFlavor.MANGO);
                }

            }

            //OUTPUT
            if (txtName.Text != "")
            {
                txtOrder.Text += anOrder.Name  + 
                    "\n------------\n";
            }
            else
            {
                lblNameError.Text = "You need a name";
            }

            
            if(hasSundae)
            { 
                if(anOrder.Sundae.ToppingCount == 0)
                {
                     
                    txtOrder.Text +=
                         
                        "Sundae - NONE " +  anOrder.Sundae.Price +
                        "\n";
                    orderTotal += anOrder.Sundae.Price;
                }
                else if(anOrder.Sundae.ToppingCount == 1)
                {
                    txtOrder.Text += 
                        "Sundae " + " - " +
                        anOrder.Sundae.GetTopping(0) + 
                        " "  + 
                        anOrder.Sundae.Price +
                        "\n";
                    orderTotal += anOrder.Sundae.Price;
                }
                else if(anOrder.Sundae.ToppingCount == 2)
                {
                    txtOrder.Text += 
                        "Sundae " + " - " +
                        anOrder.Sundae.GetTopping(0) +
                        " " +
                        anOrder.Sundae.GetTopping(1) +
                        " " +
                        anOrder.Sundae.Price+ 
                        "\n";
                    orderTotal += anOrder.Sundae.Price;
                }
                else if(anOrder.Sundae.ToppingCount == 3)
                {
                    lblSundaeError.Text = "Only 2 toppings allowed";
                }
            }
            else
            {

            }

            if(hasSoda)
            {
               txtOrder.Text += "Soda - " +
                 anOrder.Soda.Flavor + " " + 
                 anOrder.Soda.Price + "\n\n";
                 orderTotal += anOrder.Soda.Price;
            }

            lblOrderPrice.Text = orderTotal.ToString("C");

        }
    }
}
