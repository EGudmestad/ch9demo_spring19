﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB11
{
    public enum SodaFlavor
    {
        NONE,
        LIME,
        PEACH,
        MANGO
    }
}
