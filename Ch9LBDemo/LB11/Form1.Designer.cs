﻿namespace LB11
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chkSundae = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkSprinkles = new System.Windows.Forms.CheckBox();
            this.chkNuts = new System.Windows.Forms.CheckBox();
            this.chkChocolateSyrup = new System.Windows.Forms.CheckBox();
            this.lblSundaeError = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.chkSoda = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.chkLime = new System.Windows.Forms.CheckBox();
            this.chkPeach = new System.Windows.Forms.CheckBox();
            this.chkMango = new System.Windows.Forms.CheckBox();
            this.lblSodaError = new System.Windows.Forms.Label();
            this.txtOrder = new System.Windows.Forms.RichTextBox();
            this.lblOrderPrice = new System.Windows.Forms.Label();
            this.lblNameError = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(189, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "What name do you want on the order?";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(16, 29);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(215, 20);
            this.txtName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Do you want a Sundae?";
            // 
            // chkSundae
            // 
            this.chkSundae.AutoSize = true;
            this.chkSundae.Location = new System.Drawing.Point(19, 118);
            this.chkSundae.Name = "chkSundae";
            this.chkSundae.Size = new System.Drawing.Size(44, 17);
            this.chkSundae.TabIndex = 3;
            this.chkSundae.Text = "Yes";
            this.chkSundae.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 161);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Sundae Toppings";
            // 
            // chkSprinkles
            // 
            this.chkSprinkles.AutoSize = true;
            this.chkSprinkles.Location = new System.Drawing.Point(16, 188);
            this.chkSprinkles.Name = "chkSprinkles";
            this.chkSprinkles.Size = new System.Drawing.Size(69, 17);
            this.chkSprinkles.TabIndex = 5;
            this.chkSprinkles.Text = "Sprinkles";
            this.chkSprinkles.UseVisualStyleBackColor = true;
            // 
            // chkNuts
            // 
            this.chkNuts.AutoSize = true;
            this.chkNuts.Location = new System.Drawing.Point(16, 211);
            this.chkNuts.Name = "chkNuts";
            this.chkNuts.Size = new System.Drawing.Size(48, 17);
            this.chkNuts.TabIndex = 6;
            this.chkNuts.Text = "Nuts";
            this.chkNuts.UseVisualStyleBackColor = true;
            // 
            // chkChocolateSyrup
            // 
            this.chkChocolateSyrup.AutoSize = true;
            this.chkChocolateSyrup.Location = new System.Drawing.Point(16, 234);
            this.chkChocolateSyrup.Name = "chkChocolateSyrup";
            this.chkChocolateSyrup.Size = new System.Drawing.Size(104, 17);
            this.chkChocolateSyrup.TabIndex = 7;
            this.chkChocolateSyrup.Text = "Chocolate Syrup";
            this.chkChocolateSyrup.UseVisualStyleBackColor = true;
            // 
            // lblSundaeError
            // 
            this.lblSundaeError.AutoSize = true;
            this.lblSundaeError.Location = new System.Drawing.Point(19, 268);
            this.lblSundaeError.Name = "lblSundaeError";
            this.lblSundaeError.Size = new System.Drawing.Size(0, 13);
            this.lblSundaeError.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(19, 284);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 44);
            this.button1.TabIndex = 9;
            this.button1.Text = "Add item to order";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(233, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Do you want a soda?";
            // 
            // chkSoda
            // 
            this.chkSoda.AutoSize = true;
            this.chkSoda.Location = new System.Drawing.Point(236, 118);
            this.chkSoda.Name = "chkSoda";
            this.chkSoda.Size = new System.Drawing.Size(44, 17);
            this.chkSoda.TabIndex = 11;
            this.chkSoda.Text = "Yes";
            this.chkSoda.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(236, 160);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Soda Mixins";
            // 
            // chkLime
            // 
            this.chkLime.AutoSize = true;
            this.chkLime.Location = new System.Drawing.Point(236, 188);
            this.chkLime.Name = "chkLime";
            this.chkLime.Size = new System.Drawing.Size(80, 17);
            this.chkLime.TabIndex = 13;
            this.chkLime.Text = "Lime Flavor";
            this.chkLime.UseVisualStyleBackColor = true;
            // 
            // chkPeach
            // 
            this.chkPeach.AutoSize = true;
            this.chkPeach.Location = new System.Drawing.Point(236, 211);
            this.chkPeach.Name = "chkPeach";
            this.chkPeach.Size = new System.Drawing.Size(89, 17);
            this.chkPeach.TabIndex = 14;
            this.chkPeach.Text = "Peach Flavor";
            this.chkPeach.UseVisualStyleBackColor = true;
            // 
            // chkMango
            // 
            this.chkMango.AutoSize = true;
            this.chkMango.Location = new System.Drawing.Point(236, 234);
            this.chkMango.Name = "chkMango";
            this.chkMango.Size = new System.Drawing.Size(91, 17);
            this.chkMango.TabIndex = 15;
            this.chkMango.Text = "Mango Flavor";
            this.chkMango.UseVisualStyleBackColor = true;
            // 
            // lblSodaError
            // 
            this.lblSodaError.AutoSize = true;
            this.lblSodaError.Location = new System.Drawing.Point(239, 267);
            this.lblSodaError.Name = "lblSodaError";
            this.lblSodaError.Size = new System.Drawing.Size(0, 13);
            this.lblSodaError.TabIndex = 16;
            // 
            // txtOrder
            // 
            this.txtOrder.Location = new System.Drawing.Point(408, 89);
            this.txtOrder.Name = "txtOrder";
            this.txtOrder.Size = new System.Drawing.Size(218, 162);
            this.txtOrder.TabIndex = 17;
            this.txtOrder.Text = "";
            // 
            // lblOrderPrice
            // 
            this.lblOrderPrice.AutoSize = true;
            this.lblOrderPrice.Location = new System.Drawing.Point(534, 267);
            this.lblOrderPrice.Name = "lblOrderPrice";
            this.lblOrderPrice.Size = new System.Drawing.Size(0, 13);
            this.lblOrderPrice.TabIndex = 18;
            // 
            // lblNameError
            // 
            this.lblNameError.AutoSize = true;
            this.lblNameError.Location = new System.Drawing.Point(16, 56);
            this.lblNameError.Name = "lblNameError";
            this.lblNameError.Size = new System.Drawing.Size(0, 13);
            this.lblNameError.TabIndex = 19;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(661, 372);
            this.Controls.Add(this.lblNameError);
            this.Controls.Add(this.lblOrderPrice);
            this.Controls.Add(this.txtOrder);
            this.Controls.Add(this.lblSodaError);
            this.Controls.Add(this.chkMango);
            this.Controls.Add(this.chkPeach);
            this.Controls.Add(this.chkLime);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.chkSoda);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblSundaeError);
            this.Controls.Add(this.chkChocolateSyrup);
            this.Controls.Add(this.chkNuts);
            this.Controls.Add(this.chkSprinkles);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.chkSundae);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkSundae;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkSprinkles;
        private System.Windows.Forms.CheckBox chkNuts;
        private System.Windows.Forms.CheckBox chkChocolateSyrup;
        private System.Windows.Forms.Label lblSundaeError;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkSoda;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkLime;
        private System.Windows.Forms.CheckBox chkPeach;
        private System.Windows.Forms.CheckBox chkMango;
        private System.Windows.Forms.Label lblSodaError;
        private System.Windows.Forms.RichTextBox txtOrder;
        private System.Windows.Forms.Label lblOrderPrice;
        private System.Windows.Forms.Label lblNameError;
    }
}

