﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB11
{
    public class Order
    {
        //Instance Fields
        private string _name;
        private Sundae _sundae;
        private Soda _soda;

        //Properties
        public string Name
        {
            get
            {
                return _name;
            }
        }

        public Sundae Sundae
        {
            get
            {
                return _sundae;
            }
        }

        public Soda Soda
        {
            get
            {
                return _soda;
            }
        }

        //Auto Implemented Price
        public double Price { get; }

        //Constructor
        public Order(String name,bool hasSundae,bool hasSoda)
        {
            _name = name;

            if(hasSundae)
            {
                _sundae = new Sundae();
                Price += _sundae.Price;
            }

            if(hasSoda)
            {
                _soda = new Soda();
                Price += _soda.Price;
            }
        }

    }
}
